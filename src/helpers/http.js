import Vue from 'vue'
import axios from 'axios'

const token = localStorage.getItem('token')

export default Vue.prototype.$http = axios.create({
  baseURL: 'http://localhost:4000',
  headers: {
    'X-Requested-With': 'XMLHttpRequest',
    'Authorization': `Bearer ${token}`
  }
})
