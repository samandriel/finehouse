import { Validator } from 'vee-validate'

const dictionary = {
  en: {
    messages: {
      confirmed: (field) => `${field} does not match.`
    }
  },
  th: {
    messages: {
      alpha: () => 'พิมพ์ไทย'
    }
  }
}

Validator.updateDictionary(dictionary)

export default Validator
