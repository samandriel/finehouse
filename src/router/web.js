// Layouts
import Web from './../components/Web/Main'

// Views
import LandingPage from './../components/Web/LandingPage/Index'
import Login from './../components/User/Authentication'
import Register from './../components/User/Registration'

export default ({
  path: '/',
  name: 'Web',
  component: Web,
  children: [
    {
      path: 'home',
      alias: '/',
      name: LandingPage,
      component: LandingPage
    },
    {
      path: 'login',
      name: 'Login',
      component: Login
    },
    {
      path: 'register',
      name: 'Register',
      component: Register
    }
  ]
})
