// Layouts
import App from './../components/App/Main'
import Chore from './../components/App/Chore/Index.vue'
import Reminder from './../components/App/Reminder/Index.vue'
import BillsTracker from './../components/App/BillsTracker/Index.vue'
import ShoppingList from './../components/App/ShoppingList/Index.vue'

export default ({
  path: '/app',
  name: 'App',
  component: App,
  children: [
    {
      path: 'chore',
      name: 'Chore',
      component: Chore
    },
    {
      path: 'reminder',
      name: 'Reminder',
      component: Reminder
    },
    {
      path: 'bills-tracker',
      name: 'BillsTracker',
      component: BillsTracker
    },
    {
      path: 'shopping-list',
      name: 'ShoppingList',
      component: ShoppingList
    }
  ]
})
