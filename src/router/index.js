import Vue from 'vue'
import Router from 'vue-router'
import WebRoutes from './web'
import AppRoutes from './app'
import UserRoutes from './user'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    WebRoutes,
    AppRoutes,
    UserRoutes
  ]
})
