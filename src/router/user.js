// Layouts
import User from '@/components/User/Main'
import Setting from '@/components/User/Setting.vue'
import Profile from '@/components/User/Profile.vue'
import Family from '@/components/User/Family.vue'

export default ({
  path: '/user',
  name: 'User',
  component: User,
  children: [
    {
      path: 'profile',
      alias: '/user',
      name: 'Profile',
      component: Profile
    },
    {
      path: 'setting',
      name: 'Setting',
      component: Setting
    },
    {
      path: 'family',
      name: 'Family',
      component: Family
    }
  ]
})
