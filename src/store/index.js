import Vue from 'vue'
import Vuex from 'vuex'

import * as getters from './getters'
import * as mutations from './mutations'
import * as actions from './actions'

import appDrawer from './modules/app-drawer'
import user from './modules/user'
import alert from './modules/alert'

Vue.use(Vuex)

export const store = new Vuex.Store({
  state: {
    clientHeight: document.documentElement.clientHeight,
    clientWidth: document.documentElement.clientWidth
  },
  getters,
  mutations,
  actions,
  modules: {
    appDrawer,
    user,
    alert
  }
})
