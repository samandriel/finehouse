export const clientHeight = state => {
  state.clientHeight = document.documentElement.clientHeight
}

export const clientWidth = state => {
  state.clientWidth = document.documentElement.clientWidth
}

