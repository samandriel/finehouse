const state = {
  user: null,
  family: null,
  accessToken: null,
  rememberLoginSession: false,
  authDialog: false,
  authToken: null
}

const getters = {
  user: state => {
    return state.user
  },
  family: state => {
    return state.family
  },
  accessToken: state => {
    return state.accessToken
  },
  rememberLoginSession: state => {
    return state.rememberLoginSession
  },
  authDialog: state => {
    return state.authDialog
  }
}

const mutations = {
  user: (state, payload) => {
    state.user = payload.user
  },
  family: (state, payload) => {
    state.family = payload.family
  },
  accessToken: (state, payload) => {
    state.accessToken = payload
    localStorage.setItem('token', state.accessToken)
  },
  rememberLoginSession: state => {
    state.rememberLoginSession = !state.rememberLoginSession
  },
  authDialog: state => {
    state.authDialog = !state.authDialog
  }
}

const actions = {
}

export default {
  state,
  getters,
  mutations,
  actions
}
