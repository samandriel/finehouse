const state = {
  drawer: true
}

const getters = {
  drawer: state => {
    return state.drawer
  }
}

const mutations = {
  drawer: state => {
    state.drawer = !state.drawer
  }
}

export default {
  state,
  getters,
  mutations
}
