const state = {
  alertSuccessMessage: null,
  alertFailMessage: null,
  alertFail: false,
  alertSuccess: false
}

const getters = {
  alertSuccessMessage: state => {
    return state.alertSuccessMessage
  },
  alertFailMessage: state => {
    return state.alertFailMessage
  },
  alertFail: state => {
    return state.alertFail
  },
  alertSuccess: state => {
    return state.alertSuccess
  }
}

const mutations = {
  alertSuccessMessage: (state, payload) => {
    state.alertSuccessMessage = payload
    state.alertSuccess = true
  },
  alertFailMessage: (state, payload) => {
    state.alertFailMessage = payload
    state.alertFail = true
  },
  dismissSuccessMessage: state => {
    state.alertSuccess = false
    state.alertSuccessMessage = null
  },
  dismissFailMessage: state => {
    state.alertFail = false
    state.alertFailMessage = null
  }
}

const actions = {
  unauthorized: (state, router) => {
    router.push('Login')
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
